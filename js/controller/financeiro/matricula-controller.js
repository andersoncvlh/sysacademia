appFI.controller('MatriculaCtrl',

	function ($scope, $filter) {
		// body...
		$scope.pageHeader = "Test";

		$scope.alunos = [
			{matricula: "201600001", nome:"Anderson Carvalho Martins", mensalidade:"30",dataVencimento: "15"},
			{matricula: "201600002", nome:"Danillo Evangelista Cabral", mensalidade:"30",dataVencimento: "05"},
			{matricula: "201600003", nome:"Ítalo Silva", mensalidade:"30",dataVencimento: "10"},
			{matricula: "201600004", nome:"Renato Almeida", mensalidade:"30",dataVencimento: "15"}
		]

		$scope.adicionar = function (aluno) {
			// body...
			$scope.alunos.push(angular.copy(aluno));
			delete $scope.aluno;
		}

	}

);